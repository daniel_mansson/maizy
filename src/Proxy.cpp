#include "pch.h"

#include "Proxy.h"

Proxy::Proxy(double radius, double stepLength, Terrain& terrain) :
m_terrain(terrain),
m_radius(radius),
m_stepLength(stepLength),
m_currentUpdateTerrainBuffer(0),
m_drillSoundFade(0.0f),
m_shakeTime(0.0),
m_velocity(0,0),
m_smoothVelocity(0,0),
m_heavyDrill(false),
m_drillAmount(0)
{
	m_circle.setRadius((float)radius);
	m_circle.setOrigin((float)radius, (float)radius);
	m_circle.setFillColor(sf::Color(50, 20, 255, 150));
	m_circle.setOutlineColor(sf::Color::Green);
	m_circle.setPosition(0, 0);

	m_deviceCircle.setRadius((float)radius * 0.5f);
	m_deviceCircle.setOrigin((float)radius * 0.5f, (float)radius * 0.5f);
	m_deviceCircle.setFillColor(sf::Color::Transparent);
	m_deviceCircle.setOutlineColor(sf::Color::Blue);
	m_deviceCircle.setOutlineThickness(0.1f);
	m_deviceCircle.setPosition(0, 0);

	sBuffer.loadFromFile("../Sound/drill2.wav");
	drillSound.setBuffer(sBuffer);
	drillSound.setLoop(true);

	sBuffer2.loadFromFile("../Sound/grovborr.wav");
	drillSound2.setBuffer(sBuffer2);
	drillSound2.setLoop(true);
}

void Proxy::Move(const b2Vec2& delta)
{
	m_position += delta;
	setPosition(m_position);
	m_deviceCircle.setPosition(m_devicePos - m_position);
}

void Proxy::MoveTo(const b2Vec2& position)
{
	b2Vec2 vec = position - m_position;

	double len = vec.Normalize();
	
	const double stepLength = 0.005;
	const int maxSteps = 35;

	double momentum = 1.0;
	double radius = m_radius * 0.65;

	for (int i = 0; i < maxSteps; ++i)
	{
		m_terrain.UpdateCells(m_position, radius, [&momentum, vec, radius](Terrain::Cell& cell, const b2Vec2& dir, double length)
		{
			double d = b2Dot(vec, dir);
			
			if (d > 0)
				momentum -= d * 0.8 * (radius - length) * cell.durability;
		});

		if (momentum < 0)
			break;

		double step = stepLength * momentum;
		if (step > len)
			step = len;

		m_position += step * vec;
		len -= step;

		if (len < 0.000001)
			break;
	}

	setPosition(m_position);
	m_deviceCircle.setPosition(m_devicePos - m_position);
}

void Proxy::Update(double timeStep)
{
	m_currentUpdateTerrainBuffer = 1 - m_currentUpdateTerrainBuffer;
	std::this_thread::yield();

	m_terrain.UpdateTexture(m_updateTerrainBuffers[1 - m_currentUpdateTerrainBuffer]);
	m_updateTerrainBuffers[1 - m_currentUpdateTerrainBuffer].clear();


	float drillAmount = m_drillAmount;
	m_drillAmount = 0;
	/*   L�TTARE BORR */
	//Drill sound
	if (!m_heavyDrill)
	{
		if (m_buttonDown)
		{
			if (m_drillSoundFade < 1.0f)
				m_drillSoundFade += (float)timeStep * 5.0f;
			else
				m_drillSoundFade = 1.0f;

			if (drillSound.getStatus() != sf::Sound::Playing)
				drillSound.play();

			drillSound.setPitch(0.7f + drillAmount * 0.1f);
		}
		else
		{
			if (m_drillSoundFade > 5.0f * (float)timeStep)
			{
				m_drillSoundFade -= 5.0f * (float)timeStep;
			}
			else
			{
				m_drillSoundFade = 0.0f;
				if (drillSound.getStatus() == sf::Sound::Playing)
					drillSound.pause();
			}
			drillSound.setPitch(0.6f);
		}
		drillSound.setVolume(100.0f * m_drillSoundFade);
	}
	/* L�TTARE BORR SLUT */
	else
	{
		//  GROV BORR
		//Drill sound
		if (m_buttonDown)
		{
			if (m_drillSoundFade < 1.0f)
				m_drillSoundFade += (float)timeStep * 5.0f;
			else
				m_drillSoundFade = 1.0f;

			if (drillSound2.getStatus() != sf::Sound::Playing)
				drillSound2.play();

			drillSound2.setPitch(0.7f + drillAmount * 0.1f);
		}
		else
		{
			if (m_drillSoundFade > 0.0f)
				m_drillSoundFade -= 5.0f * (float)timeStep;
			else
			{
				m_drillSoundFade = 0.0f;
				if (drillSound2.getStatus() == sf::Sound::Playing)
					drillSound2.pause();
			}
			drillSound2.setPitch(0.6f);
		}
		drillSound2.setVolume(60.0f * m_drillSoundFade);
	}
}

void Proxy::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

//	target.draw(m_circle, states);
	//target.draw(m_deviceCircle, states);
}

void Proxy::UpdateHaptics(chai3d::cVector3d& force, chai3d::cGenericHapticDevicePtr device, double timeStep)
{
	chai3d::cVector3d devicePos;
	device->getPosition(devicePos);

	b2Vec2 pos(devicePos.y(), devicePos.z());
	pos *= (240.0 / 0.06);
	b2Vec2 lastPosition = m_position;
	m_devicePos = pos;

	b2Vec2 oldPos = m_position;
	m_velocity = m_devicePos - m_position;

	MoveTo(pos);
	double radius = m_radius;
	bool change = false;
	bool buttonDown = false;
	float drillAmount = 0;
	device->getUserSwitch(0, buttonDown);

	m_terrain.UpdateCells(m_position, m_radius, [&change, &force, &drillAmount, radius, buttonDown, timeStep](Terrain::Cell& cell, const b2Vec2& dir, double length)
	{
		if (cell.durability > 0)
		{
			//Inside inner radius
			if (buttonDown && length < radius * 0.8)
			{
				float t = (float)b2Min((1.0 - length / (radius * 0.8)) * 7.0, 1.0);
				//Lower durability
				float amount = t * 10.0f * (float)timeStep;
				cell.durability -= amount;
				drillAmount += amount;
				//Flag terrain as dirty
				change = true;
			}

			//Apply forces
			b2Vec2 f = -0.4 * cell.durability * (radius - length) * dir;
			force(1) += f.x;
			force(2) += f.y;
		}
	});

	Move(0.004 * b2Vec2(force(1), force(2)));

	//Proxy offset force
	b2Vec2 offsetForce = 0.5 * (m_position - m_devicePos);
	double offsetLen = offsetForce.Normalize();

	if (offsetLen > 1.7)
	{
		if (offsetLen > 8.7)
			offsetLen = 8.7;

		offsetForce *= offsetLen - 1.7;
		
		force(1) += offsetForce.x;
		force(2) += offsetForce.y;

		//std::cout << offsetForce.Length() << "\n";
	}

	m_buttonDown = buttonDown;
	m_drillAmount += drillAmount;

	//Drill shake
	if (buttonDown)
	{
		m_shakeTime += timeStep;

		b2Vec2 shake(cos(m_shakeTime * 400.0 * m_drillSoundFade), sin(m_shakeTime * 540.0 * m_drillSoundFade));

		shake *= m_drillSoundFade * ((double)drillAmount + 0.05) * 7.0;

		force(0) += 0.5 * (shake.x + shake.y);
		force(1) += shake.x;
		force(2) += shake.y;
	}

	if (change)
	{
		m_updateTerrainBuffers[m_currentUpdateTerrainBuffer].push_back(std::pair<b2Vec2, double>(m_position, m_radius));
	}

	m_smoothVelocity += 5.0 * timeStep * ((m_position - oldPos) - m_smoothVelocity);
}

