#pragma once

class Highscore : public sf::Drawable
{
public:
	Highscore();

	void Load(const std::string& filename);
	void Save(const std::string& filename);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void Update(double timeStep);
	bool HandleEvent(sf::Event event);

	void StartNewEntry(int time);

	void SetIsActive(bool active) { m_isActive = active; }
	bool IsActive() const { return m_isActive; }

private:

	struct Entry
	{
		int time;
		std::string name;
		sf::Text nameText;
		sf::Text timeText;
	};

	bool m_isActive;
	std::multimap<int, Entry> m_entries;

	sf::Font m_font;
	sf::Text m_header;
	sf::View m_view;

	Entry* m_current;
};