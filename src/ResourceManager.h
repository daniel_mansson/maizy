#pragma once

class ResourceManager
{
public:
	template<typename T>
	std::shared_ptr<T> Get(const std::string& id) const;

	template<typename T>
	void Put(const std::string& id, T* data);

private:
	class Resource;
	typedef std::unique_ptr<Resource> ResourcePtr;

	template <typename T>
	class ResourceImpl;

	std::map<std::string, ResourcePtr> m_resources;
};

class ResourceManager::Resource
{
public:
	virtual ~Resource(){}
};

template <typename T>
class ResourceManager::ResourceImpl : public ResourceManager::Resource
{
public:
	ResourceImpl(T* data) : m_data(data) {}

	std::shared_ptr<T> GetData() const { return m_data; };
private:
	std::shared_ptr<T> m_data;
};

template<typename T>
std::shared_ptr<T> ResourceManager::Get(const std::string& id) const
{
	auto it = m_resources.find(id);

	if (it == m_resources.end())
		return nullptr;
	else
		return dynamic_cast<ResourceImpl<T>*>(it->second.get())->GetData();
}

template<typename T>
void ResourceManager::Put(const std::string& id, T* data)
{
	auto resource = new ResourceImpl<T>(data);
	auto it = m_resources.lower_bound(id);
	
	if (it != m_resources.end() && !m_resources.key_comp()(id, it->first))
	{
		std::cout << "Warning. Overwriting resource: " << id << std::endl;
		it->second = ResourcePtr(resource);
	}
	else
	{
		m_resources.insert(it, std::make_pair(id, ResourcePtr(resource)));
	}
}

