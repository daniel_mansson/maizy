#include "pch.h"

#include "Application.h"

#include "HelloWorldScene.h"
#include "Global.h"
#include "ResourceManager.h"
#include "DummyDevice.h"

bool Application::Initialize()
{
	srand((unsigned int)time(NULL));

	// TODO: Config file
	m_window.create(sf::VideoMode(2160, 1440), "Amazing Maizy", sf::Style::Titlebar | sf::Style::Close );

	m_hapticDeviceHandler = std::unique_ptr<chai3d::cHapticDeviceHandler>(new chai3d::cHapticDeviceHandler());

	for (unsigned int i = 0; i < m_hapticDeviceHandler->getNumDevices(); ++i)
	{
		chai3d::cGenericHapticDevicePtr hapticDevice = 0;

		if (!m_hapticDeviceHandler->getDevice(hapticDevice, i))
		{
			std::cerr << "Failed to get device " << i << std::endl;
			return false;
		}

		hapticDevice->open();
		hapticDevice->calibrate();

		m_devices.push_back(hapticDevice);
	}

	if (m_devices.size() == 0)
	{
		m_dummyDevice = chai3d::cGenericHapticDevicePtr(new DummyDevice(m_window));
		m_devices.push_back(m_dummyDevice);
	}
	else
	{
		m_dummyDevice = nullptr;
	}

	sf::Image* image = new sf::Image();
	image->loadFromFile("resources/stone.png");
	Global::Instance().GetResourceManager().Put("stone.png", image);

	image = new sf::Image();
	image->loadFromFile("resources/ground.png");
	Global::Instance().GetResourceManager().Put("ground.png", image);

	image = new sf::Image();
	image->loadFromFile("resources/char1.png");
	Global::Instance().GetResourceManager().Put("char1.png", image);

	image = new sf::Image();
	image->loadFromFile("resources/chest.png");
	Global::Instance().GetResourceManager().Put("chest.png", image);

	image = new sf::Image();
	image->loadFromFile("resources/amazing_maizy.png");
	Global::Instance().GetResourceManager().Put("amazing_maizy.png", image);

	image = new sf::Image();
	image->loadFromFile("resources/diamondball.png");
	Global::Instance().GetResourceManager().Put("diamondball.png", image);

	image = new sf::Image();
	image->loadFromFile("resources/goldenball.png");
	Global::Instance().GetResourceManager().Put("goldenball.png", image);

	image = new sf::Image();
	image->loadFromFile("resources/silverball.png");
	Global::Instance().GetResourceManager().Put("silverball.png", image);

	//Setup root scene
	m_sceneManager.Push(ScenePtr(new HelloWorldScene(m_sceneManager)));

	return true;
}

void Application::HapticsThread(void* applicationPtr)
{
	Application& application = *static_cast<Application*>(applicationPtr);

	chai3d::cPrecisionClock clock;
	while (application.m_isRunning)
	{
		clock.stop();
		double timeStep = clock.getCurrentTimeSeconds();
		clock.start(true);

		if (application.m_dummyDevice != nullptr)
			static_cast<DummyDevice*>(application.m_dummyDevice.get())->UpdateHaptics(timeStep);

		application.m_sceneManager.UpdateHaptics(application.m_devices, timeStep);
	}

	application.m_hapticThreadFinished = true;
}

void Application::Run()
{
	m_isRunning = true;
	m_hapticThreadFinished = false;
	std::unique_ptr<chai3d::cThread> thread = std::unique_ptr<chai3d::cThread>(new chai3d::cThread());
	thread->start(&HapticsThread, chai3d::CTHREAD_PRIORITY_HAPTICS, this);

	chai3d::cPrecisionClock clock;
	while (m_isRunning)
	{
		sf::Event event;
		while (m_window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				m_isRunning = false;
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape)
					m_isRunning = false;
				break;
			}

			m_sceneManager.HandleEvent(event);
		}

		clock.stop();
		double timeStep = clock.getCurrentTimeSeconds();
		clock.start(true);

		if (m_dummyDevice != nullptr)
			static_cast<DummyDevice*>(m_dummyDevice.get())->UpdateGraphics(timeStep);

		m_sceneManager.UpdateGraphics(m_window, timeStep);
	}

	while (!m_hapticThreadFinished)
	{
		chai3d::cSleepMs(10);
	}

	m_window.close();
}

