#pragma once

class Scene;
typedef std::shared_ptr<Scene> ScenePtr;

class SceneStack
{
public:
	SceneStack(){}
	virtual ~SceneStack(){}

	virtual void Push(ScenePtr scene) = 0;
	virtual ScenePtr Pop() = 0;
	virtual ScenePtr Top() = 0;
};