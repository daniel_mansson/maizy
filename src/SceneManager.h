#pragma once

#include "SceneStack.h"

class SceneManager : public SceneStack
{
public:
	SceneManager();
	~SceneManager();

	virtual void Push(ScenePtr scene);
	virtual ScenePtr Pop();
	virtual ScenePtr Top();

	void HandleEvent(const sf::Event& event);
	void UpdateGraphics(sf::RenderWindow& window, const double& timeStep);
	void UpdateHaptics(const std::vector<chai3d::cGenericHapticDevicePtr>& devices, const double& timeStep);

private:
	std::vector<ScenePtr> m_sceneStack;
};