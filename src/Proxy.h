#pragma once

#include "Terrain.h"
#include "DiamondCircle.h"
#include "GoldenCircle.h"
#include "SilverCircle.h"

class Proxy : public sf::Drawable, public sf::Transformable
{
public:
	Proxy(double radius, double stepLength, Terrain& terrain);

	void Move(const b2Vec2& delta);
	void MoveTo(const b2Vec2& position);

	void Update(double timeStep);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void UpdateHaptics(chai3d::cVector3d& force, chai3d::cGenericHapticDevicePtr device, double timeStep);
	bool ButtonStatus();

	const b2Vec2& getVelocity() const { return m_velocity; }
	const b2Vec2& getSmoothVelocity() const { return m_smoothVelocity; }

	void ToggleDrill() 
	{
		m_heavyDrill = !m_heavyDrill;
		drillSound.setVolume(0.0f);
		drillSound2.setVolume(0.0f);
	}

private:
	Terrain& m_terrain;
	double m_stepLength;
	double m_radius;
	float m_drillSoundFade;
	double m_shakeTime;
	b2Vec2 m_moveTarget;
	b2Vec2 m_position;
	b2Vec2 m_devicePos;

	sf::CircleShape m_circle;
	sf::CircleShape m_deviceCircle;

	sf::SoundBuffer sBuffer;
	sf::Sound drillSound;
	bool m_heavyDrill;

	sf::SoundBuffer sBuffer2;
	sf::Sound drillSound2;
	b2Vec2 m_velocity;
	b2Vec2 m_smoothVelocity;

	volatile bool m_buttonDown;
	volatile float m_drillAmount;

	volatile int m_currentUpdateTerrainBuffer;
	std::vector<std::pair<b2Vec2, double>> m_updateTerrainBuffers[2];
};