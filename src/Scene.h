#pragma once

class Scene;
typedef std::shared_ptr<Scene> ScenePtr;

class SceneStack;

class Scene
{
public:
	Scene(SceneStack& sceneStack);
	virtual ~Scene();

	virtual void OnEnter(ScenePtr from) = 0;
	virtual void OnExit(ScenePtr to) = 0;

	virtual void HandleEvent(sf::Event event) = 0;
	virtual void UpdateGraphics(sf::RenderWindow& window, double timeStep) = 0;
	virtual void UpdateHaptics(std::vector<chai3d::cGenericHapticDevicePtr> devices, double timeStep) = 0;

protected:
	SceneStack& m_sceneStack;
};
