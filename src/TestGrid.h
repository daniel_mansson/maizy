#pragma once

class TestGrid : public sf::Drawable, public sf::Transformable
{
public:
	struct Cell
	{
		b2Vec2 pos;
		float durability;
	};

	TestGrid(int width, int height, double cellSize);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;


	void UpdateHaptics(std::vector<chai3d::cGenericHapticDevicePtr> devices, double timeStep);


private:
	int m_width;
	int m_height;
	double m_cellSize;
	std::vector<sf::RectangleShape> m_shapes;	
	std::vector<Cell> m_cells;
	sf::CircleShape m_proxy;
	sf::Sound drillSound;
};