#pragma once

#include "Terrain.h"

class GoldenCircle
{
public:
	GoldenCircle(Terrain& terrain);
	void UpdateHaptics(chai3d::cVector3d& force, double timeStep);
	void Move(b2Vec2 vec);
	void setValues(float x, float y);
	void clearTerrain();
	sf::CircleShape getCircle();
	b2Vec2 getForce(const b2Vec2);
	float getSound(const b2Vec2);
	void UpdateGraphics();
	bool found;
private:
	Terrain& m_terrain;

protected:
	sf::CircleShape m_gcshape;

};