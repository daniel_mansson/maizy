#pragma once

class Circle
{
public:
	Circle();
	void UpdateHaptics(chai3d::cVector3d& force, double timeStep);

	void setValues(float rad, sf::Color fillcol,  float x, float y);
	sf::CircleShape getCircle();
	chai3d::cVector3d getForce(const b2Vec2, float rad);

protected:
	sf::CircleShape m_cshape;

};