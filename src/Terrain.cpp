#include "pch.h"
#include "Terrain.h"

#include "Global.h"
#include "ResourceManager.h"

Terrain::Terrain(int width, int height, double cellSize) :
m_width(width),
m_height(height),
m_cellSize(cellSize)
{
	m_cells.resize(width * height);

	m_stoneTexture.loadFromImage(*Global::Instance().GetResourceManager().Get<sf::Image>("stone.png"));
	m_stoneTexture.setRepeated(true);

	m_texture.create(width, height);
	m_pixels = std::unique_ptr<sf::Color[]>(new sf::Color[width * height]);

	for (int i = 0; i < height; ++i)
	{
		for (int j = 0; j < width; ++j)
		{
			m_cells[i * width + j].pos = b2Vec2(j * cellSize - cellSize * width * 0.5, i * cellSize - cellSize * height * 0.5);
			m_cells[i * width + j].durability = 1.0f;
			m_pixels[i * width + j] = sf::Color::White;
		}
	}

	m_texture.update((sf::Uint8*)m_pixels.get());
	m_texture.setSmooth(true);
	m_sprite.setTexture(m_texture);

	m_sprite.setScale((float)cellSize, (float)cellSize);
	m_sprite.setOrigin(width * 0.5f, height * 0.5f);

	m_shader.loadFromFile("resources/test.frag", sf::Shader::Fragment);
	m_shader.setParameter("texture", sf::Shader::CurrentTexture);
	m_shader.setParameter("stone", m_stoneTexture);
}

void Terrain::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	states.shader = &m_shader;
	target.draw(m_sprite, states);
}

void Terrain::UpdateCells(const b2Vec2& pos, double radius, std::function<void(Cell&, const b2Vec2&, double)> callback)
{
	int left = (int)((pos.x - radius + (float)m_cellSize * m_width * 0.5f) / m_cellSize);
	int right = (int)((pos.x + radius + (float)m_cellSize * m_width * 0.5f) / m_cellSize) + 1;
	int bottom = (int)((pos.y - radius + (float)m_cellSize * m_height * 0.5f) / m_cellSize);
	int top = (int)((pos.y + radius + (float)m_cellSize * m_height * 0.5f) / m_cellSize) + 1;

	left = chai3d::cClamp(left, 0, m_width);
	right = chai3d::cClamp(right, 0, m_width);
	bottom = chai3d::cClamp(bottom, 0, m_height);
	top = chai3d::cClamp(top, 0, m_height);

	double radiusSq = radius * radius;
	b2Vec2 force(0, 0);
	for (int i = bottom; i < top; ++i)
	{
		for (int j = left; j < right; ++j)
		{
			b2Vec2 to = m_cells[i * m_width + j].pos - pos;
			double lenSq = to.LengthSquared();
			if (lenSq < radiusSq)
			{
				double len = to.Normalize();
				callback(m_cells[i * m_width + j], to, len);
			}
		}
	}
}

void Terrain::UpdateTexture(int left, int right, int bottom, int top)
{
	left = chai3d::cClamp(left, 0, m_width);
	right = chai3d::cClamp(right, 0, m_width);
	bottom = chai3d::cClamp(bottom, 0, m_height);
	top = chai3d::cClamp(top, 0, m_height);

	int w = right - left;
	int h = top - bottom;
	for (int i = bottom; i < top; ++i)
	{
		for (int j = left; j < right; ++j)
		{
			float dur = m_cells[i * m_width + j].durability;
			if (dur < 0)
				dur = 0;
			sf::Uint8 c = (sf::Uint8)(255.0 * dur);
			m_pixels[(i - bottom) * w + (j - left)] = sf::Color(255, 255, 255, c);
		}
	}

	m_texture.update((sf::Uint8*)m_pixels.get(), w, h, left, bottom);
}

void Terrain::UpdateTexture(const b2Vec2& pos, double radius)
{
	int left = (int)((pos.x - radius + (float)m_cellSize * m_width * 0.5f) / m_cellSize);
	int right = (int)((pos.x + radius + (float)m_cellSize * m_width * 0.5f) / m_cellSize) + 1;
	int bottom = (int)((pos.y - radius + (float)m_cellSize * m_height * 0.5f) / m_cellSize);
	int top = (int)((pos.y + radius + (float)m_cellSize * m_height * 0.5f) / m_cellSize) + 1;

	UpdateTexture(left, right, bottom, top);
}

void Terrain::UpdateTexture(const std::vector<std::pair<b2Vec2, double>>& areas)
{
	int left = m_width;
	int right = 0;
	int bottom = m_height;
	int top = 0;

	for (auto p : areas)
	{
		int i;

		i = (int)((p.first.x - p.second + (float)m_cellSize * m_width * 0.5f) / m_cellSize);
		if (i < left)
			left = i;

		i = (int)((p.first.x + p.second + (float)m_cellSize * m_width * 0.5f) / m_cellSize);
		if (i > right)
			right = i;

		i = (int)((p.first.y - p.second + (float)m_cellSize * m_height * 0.5f) / m_cellSize);
		if (i < bottom)
			bottom = i;

		i = (int)((p.first.y + p.second + (float)m_cellSize * m_height * 0.5f) / m_cellSize);
		if (i > top)
			top = i;
	}

	if (left <= right && bottom <= top)
		UpdateTexture(left, right, bottom, top);
}

bool Terrain::RayCast(const b2Vec2& from, const b2Vec2& dir, double& distance, double threshold, int maxSteps) const
{
	double stepLength = m_cellSize * 0.5;
	b2Vec2 vec = stepLength * dir;
	distance = 0.0;

	b2Vec2 p = from;
	
	for (int s = 0; s < maxSteps; ++s)
	{
		int i = (int)((p.y + m_cellSize * m_height * 0.5) / m_cellSize);
		int j = (int)((p.x + m_cellSize * m_width * 0.5) / m_cellSize);

		if (!(j < 0 || j >= m_width || i < 0 || i >= m_height))
		{
			float durability = m_cells[i * m_width + j].durability;
			if (durability > threshold)
				return true;
		}

		distance += stepLength;
		p += vec;
	}

	return false;
}