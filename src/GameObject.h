#pragma once

class GameObject
{
public:
	virtual ~GameObject(){}

	virtual void UpdateGraphics(sf::RenderWindow& window, double timeStep) = 0;
	virtual void UpdateHaptics(std::vector<chai3d::cGenericHapticDevicePtr> devices, double timeStep) = 0;
};