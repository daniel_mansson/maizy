#pragma once

class Character : public sf::Drawable, public sf::Transformable
{
public:
	Character();
	
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void setImage(const sf::Image& image, int tileSize);
	void update(double timeStep);
	void setDirection(const b2Vec2& direction);

	void setAnimationSpeed(double speed) { m_speed = speed; }

private:
	sf::Sprite m_sprite;
	sf::Texture m_texture;
	double m_speed;
	double m_time;
	int m_currentSprite;
	int m_tileSize;
	int m_dir;
};