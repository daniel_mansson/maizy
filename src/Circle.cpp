#include "pch.h"
#include "Circle.h"

Circle::Circle() 
{
	//std::cout << "circle" << std::endl;
}

void Circle::setValues(float rad, sf::Color fillcol, float x, float y)
{
	m_cshape.setOrigin(rad, rad);
	m_cshape.setRadius(rad);
	m_cshape.setFillColor(fillcol);
	m_cshape.setPosition(x, y);
}

sf::CircleShape Circle::getCircle() 
{
	return m_cshape;
}

void Circle::UpdateHaptics(chai3d::cVector3d& force, double timeStep)
{
	
}


chai3d::cVector3d Circle::getForce(const b2Vec2 pos, float rad)
{

	chai3d::cVector3d force(0, 0, 0);
	b2Vec2 ballPos(m_cshape.getPosition().x, m_cshape.getPosition().y);
	
	if ((ballPos.Length() - pos.Length()) <= 0){

		// what values to take in? y, x, z??
		force = chai3d::cVector3d(0, pos.y * 1000, pos.x * 1000);
		return force;
	}
	else {
		return force;
	}



}

