#pragma once

#include "SceneManager.h"

class DummyDevice;

class Application
{
public:
	bool Initialize();
	void Run();

private:
	SceneManager m_sceneManager;

	sf::RenderWindow m_window;

	std::vector<chai3d::cGenericHapticDevicePtr> m_devices;
	std::unique_ptr<chai3d::cHapticDeviceHandler> m_hapticDeviceHandler;
	chai3d::cGenericHapticDevicePtr m_dummyDevice;

	volatile bool m_isRunning;
	volatile bool m_hapticThreadFinished;

	static void HapticsThread(void* applicationPtr);
};
