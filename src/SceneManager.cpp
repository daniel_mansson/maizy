#include "pch.h"

#include "SceneManager.h"
#include "Scene.h"

SceneManager::SceneManager()
{

}

SceneManager::~SceneManager()
{

}

void SceneManager::Push(ScenePtr scene)
{
	ScenePtr prevTop = Top();
	m_sceneStack.push_back(scene);

	if(prevTop != nullptr)
		prevTop->OnExit(scene);

	scene->OnEnter(prevTop);
}

ScenePtr SceneManager::Pop()
{
	ScenePtr prevTop = Top();

	if (!m_sceneStack.empty())
		m_sceneStack.pop_back();

	ScenePtr top = Top();

	if (prevTop != nullptr)
		prevTop->OnExit(top);

	if (top != nullptr)
		top->OnEnter(prevTop);

	return prevTop;
}

ScenePtr SceneManager::Top()
{
	return m_sceneStack.empty() ? nullptr : m_sceneStack.back();
}

void SceneManager::HandleEvent(const sf::Event& event)
{
	if (!m_sceneStack.empty())
		m_sceneStack.back()->HandleEvent(event);
}

void SceneManager::UpdateGraphics(sf::RenderWindow& window, const double& timeStep)
{
	if (!m_sceneStack.empty())
		m_sceneStack.back()->UpdateGraphics(window, timeStep);
}

void SceneManager::UpdateHaptics(const std::vector<chai3d::cGenericHapticDevicePtr>& devices, const double& timeStep)
{
	if (!m_sceneStack.empty())
		m_sceneStack.back()->UpdateHaptics(devices, timeStep);
}




