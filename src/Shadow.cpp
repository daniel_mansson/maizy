#include "pch.h"

#include "Shadow.h"

#include "Terrain.h"

Shadow::Shadow(int rays) :
m_rays(rays),
m_position(0,0)
{
	m_texture.create(rays, 1);
	m_texture.setRepeated(true);
	m_texture.setSmooth(true);

	m_shader.loadFromFile("resources/shadow.frag", sf::Shader::Fragment);
	m_shader.setParameter("shadowTex", m_texture);
	m_shader.setParameter("position", sf::Vector2f(0.5f, 0.5f));

	m_fullscreenQuad = sf::RectangleShape(sf::Vector2f(400.0f, 400.0f));
	m_fullscreenQuad.setOrigin(200.0f, 200.0f);
	m_fullscreenQuad.setTexture(&m_texture);

	m_rayData = std::unique_ptr<sf::Color[]>(new sf::Color[rays]);
	for (int i = 0; i < rays; ++i)
	{
		m_rayData[i] = sf::Color(0, 0, 0, 0);
	}

	m_texture.update((sf::Uint8*)m_rayData.get());
}

Shadow::~Shadow()
{

}

void Shadow::Update(const b2Vec2& position, const Terrain& terrain)
{ 
	for (int i = 0; i < m_rays; ++i)
	{
		double angle = 2.0 * b2_pi * (double)i / (double)m_rays;
		b2Vec2 dir(cos(angle), sin(angle));

		double dist = 0;
		bool hit = terrain.RayCast(position, dir, dist);

		int value = hit ? (int)(dist*4.0) : 255;
		if (value > 255)
			value = 255;

		m_rayData[i] = sf::Color((sf::Uint8)value, 0, 0, 0);
	}

	m_texture.update((sf::Uint8*)m_rayData.get());
	m_shader.setParameter("position", 0.0025 * position + b2Vec2(0.5, 0.5));
}

void Shadow::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.shader = &m_shader;

	target.draw(m_fullscreenQuad, states);
}
