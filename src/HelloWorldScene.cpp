#include "pch.h"

#include "HelloWorldScene.h"

#include "SceneStack.h"

#include "Global.h"
#include "ResourceManager.h"
#include "Terrain.h"

HelloWorldScene::HelloWorldScene(SceneStack& sceneStack) :
Scene(sceneStack),
m_grid(300, 260, 0.3),
m_smoothedGraphicsDelta(0.1),
m_smoothedHapticsDelta(0.1),
m_terrain(800, 800, 0.5),
m_proxy(3.0, 0.5, m_terrain),
m_goldencircle(m_terrain),
m_diamondcircle(m_terrain),
m_silvercircle(m_terrain),
m_lastForce(0,0),
m_shadow(256),
m_displayShadows(true),
m_hasStarted(false),
gameover(false),
m_useSound(true),
m_closeUp(true)
{
	if (!m_font.loadFromFile("resources/SourceSansPro-Regular.ttf"))
		std::cerr << "Failed to load font." << std::endl;

	m_text.setPosition(0, 0);
	m_text.setColor(sf::Color(13,13,130));
	m_text.setFont(m_font);
	m_text.setString("asdfasdfasdfasdf");

	m_timeText.setPosition(0, 50);
	m_timeText.setColor(sf::Color(200, 200, 200));
	m_timeText.setFont(m_font);
	m_timeText.setScale(1.2f, 1.2f);
	m_timeText.setString("00:00");

	double randGoldXpos = ((rand() % 90) + 30) * (pow(-1.0, (rand() % 2 + 1)));
	double randGoldYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
	double randSilverXpos = ((rand() % 90) + 30) * (pow(-1.0, (rand() % 2 + 1)));
	double randSilverYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
	double randDiamondXpos = ((rand() % 90) + 30) * (pow(-1.0, (rand() % 2 + 1)));
	double randDiamondYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
	gameover = false;

	 bool pos= setCirclePos(randGoldXpos, randGoldYpos, randSilverXpos, randSilverYpos, randDiamondXpos, randDiamondYpos);
	
	 double angle = 2 * b2_pi * ((double)rand() / (double)RAND_MAX);
	 b2Vec2 goldPos(cos(angle), sin(angle));
	 goldPos *= 1.0 - 0.1 * ((double)rand() / (double)RAND_MAX);

	 angle = 2 * b2_pi * ((double)rand() / (double)RAND_MAX);
	 b2Vec2 silverPos(cos(angle), sin(angle));
	 silverPos *= 1.0 - 0.1 * ((double)rand() / (double)RAND_MAX);

	 angle = 2 * b2_pi * ((double)rand() / (double)RAND_MAX);
	 b2Vec2 diamondPos(cos(angle), sin(angle));
	 diamondPos *= 1.0 - 0.1 * ((double)rand() / (double)RAND_MAX);

	if (pos)
	{
		m_goldencircle.setValues( (float)(80*goldPos.x),    (float)(60*goldPos.y));
		m_silvercircle.setValues( (float)(100*silverPos.x),  (float)(70*silverPos.y));
		m_diamondcircle.setValues((float)(140*diamondPos.x), (float)(90*diamondPos.y));
	}

	m_goldencircle.clearTerrain();
	m_diamondcircle.clearTerrain();
	m_silvercircle.clearTerrain();

	dBuffer.loadFromFile("../Sound/diamond.aiff");
	diamondSound.setBuffer(dBuffer);
	diamondSound.setLoop(true);
	diamondSound.setVolume(0);

	sBuffer.loadFromFile("../Sound/silver.aiff");
	silverSound.setBuffer(sBuffer);
	silverSound.setLoop(true);
	silverSound.setVolume(0);

	gBuffer.loadFromFile("../Sound/gold.wav");
	goldSound.setBuffer(gBuffer);
	goldSound.setLoop(true);
	goldSound.setVolume(0);

	gameoverBuffer.loadFromFile("../Sound/superpower.wav");
	gameoverSound.setBuffer(gameoverBuffer);
	gameoverSound.setVolume(0);

	m_backgroundTexture.loadFromImage(*Global::Instance().GetResourceManager().Get<sf::Image>("ground.png"));
	m_backgroundTexture.setRepeated(true);
	m_backgroundTexture.setSmooth(true);

	m_chestTexture.loadFromImage(*Global::Instance().GetResourceManager().Get<sf::Image>("chest.png"));
	m_chestTexture.setSmooth(true);

	m_chest.setTexture(&m_chestTexture);
	m_chest.setRadius(5.0f);
	m_chest.setPosition(-5.0f, -5.0f);

	m_titleTexture.loadFromImage(*Global::Instance().GetResourceManager().Get<sf::Image>("amazing_maizy.png"));
	m_titleTexture.setSmooth(true);

	m_titleImage.setTexture(&m_titleTexture);
	m_titleImage.setSize(sf::Vector2f(200,50));
	m_titleImage.setPosition(10.0f, 10.0f);

	m_goldenballTexture.loadFromImage(*Global::Instance().GetResourceManager().Get<sf::Image>("goldenball.png"));
	m_goldenballTexture.setSmooth(true);

	m_goldenballImage.setTexture(&m_goldenballTexture);
	m_goldenballImage.setRadius(15.0f);
	

	m_silverballTexture.loadFromImage(*Global::Instance().GetResourceManager().Get<sf::Image>("silverball.png"));
	m_silverballTexture.setSmooth(true);

	m_silverballImage.setTexture(&m_silverballTexture);
	m_silverballImage.setRadius(15.0f);
	//m_silverballImage.setPosition(50.0f, 100.0f);

	m_diamondballTexture.loadFromImage(*Global::Instance().GetResourceManager().Get<sf::Image>("diamondball.png"));
	m_diamondballTexture.setSmooth(true);

	m_diamondballImage.setTexture(&m_diamondballTexture);
	m_diamondballImage.setRadius(15.0f);
	//m_diamondballImage.setPosition(90.0f, 100.0f);

	m_background.setTexture(m_backgroundTexture);
	m_background.setScale(400.0f / (float)m_backgroundTexture.getSize().x, 400.0f / (float)m_backgroundTexture.getSize().y);
	m_background.setOrigin(0.5f * (float)m_backgroundTexture.getSize().x, 0.5f * (float)m_backgroundTexture.getSize().y);
	m_background.setTextureRect(sf::IntRect(0, 0, m_backgroundTexture.getSize().x, m_backgroundTexture.getSize().y));
	
	m_backgroundShader.loadFromFile("resources/ground.frag", sf::Shader::Fragment);
	m_backgroundShader.setParameter("texture", sf::Shader::CurrentTexture);

	m_backgroundStates = sf::RenderStates::Default;
	m_backgroundStates.shader = &m_backgroundShader;

	m_character.setImage(*Global::Instance().GetResourceManager().Get<sf::Image>("char1.png"), 32);

	double startRadius = 20.0;
	m_terrain.UpdateCells(b2Vec2(0, 0), startRadius, [startRadius](Terrain::Cell& cell, const b2Vec2&, double length)
	{
		cell.durability = (float)b2Max(1.0 - (startRadius - length) * 5.0, 0.0);
	});
	m_terrain.UpdateTexture(b2Vec2(0, 0), startRadius);

	m_highscore.Load("resources/highscore.txt");
}


HelloWorldScene::~HelloWorldScene()
{

}

void HelloWorldScene::OnEnter(ScenePtr from)
{
	m_time = 0;
}

void HelloWorldScene::OnExit(ScenePtr to)
{

}

void HelloWorldScene::HandleEvent(sf::Event event)
{
	if (m_highscore.HandleEvent(event))
	{
		m_highscore.Save("resources/highscore.txt");
	}

	if (event.type == sf::Event::KeyPressed)
	{
		if (event.key.code == sf::Keyboard::F1)
		{
			ScenePtr scene = m_sceneStack.Pop();
			m_sceneStack.Push(ScenePtr(new HelloWorldScene(m_sceneStack)));
		}

		if (event.key.code == sf::Keyboard::F8)
		{
			m_displayShadows = !m_displayShadows;
		}


		if (event.key.code == sf::Keyboard::F2)
		{
			m_closeUp = !m_closeUp;
		}

		if (event.key.code == sf::Keyboard::F4)
		{
			m_proxy.ToggleDrill();
		}

		if (event.key.code == sf::Keyboard::F3)
		{
			m_useSound = !m_useSound;
			if (!m_useSound)
			{
				goldSound.stop();
				silverSound.stop();
				diamondSound.stop();
			}
		}
	}
}

void HelloWorldScene::UpdateGraphics(sf::RenderWindow& window, double timeStep)
{
	m_smoothedGraphicsDelta += (timeStep - m_smoothedGraphicsDelta) * 0.1;

	//Set view to world space
	if (!m_closeUp)
		window.setView(sf::View(sf::Vector2f(0, 0), sf::Vector2f(200.0f * window.getSize().x / window.getSize().y, -200.0f)));
	else
		window.setView(sf::View(m_proxy.getPosition(), sf::Vector2f(80.0f * window.getSize().x / window.getSize().y, -80.0f)));

	window.clear();
	window.draw(m_background, m_backgroundStates);
	window.draw(m_chest);

	window.draw(m_goldencircle.getCircle());
	window.draw(m_silvercircle.getCircle());
	window.draw(m_diamondcircle.getCircle());

	m_proxy.Update(timeStep);
	window.draw(m_terrain);
	window.draw(m_proxy);


	//Draw force vector
	/*sf::RectangleShape line(sf::Vector2f((float)m_lastForce.Length(), 0.6f));
	line.setOrigin(0, 0.3f);
	line.rotate((float)b2Atan2(m_lastForce.y, m_lastForce.x) * 180.0f / (float)b2_pi);
	line.setPosition(m_proxy.getPosition());
	line.setFillColor(sf::Color::Red);
	window.draw(line);*/

	m_shadow.Update(b2Vec2(m_proxy.getPosition()), m_terrain);
	if (m_displayShadows)
	{
		window.draw(m_shadow);
	}

	if (m_proxy.getVelocity().Length() > 0.1)
		m_character.setDirection(m_proxy.getSmoothVelocity());

	m_character.setPosition(m_proxy.getPosition());
	m_character.update(timeStep);
	m_character.setAnimationSpeed(m_proxy.getSmoothVelocity().Length() * 500.0);
	window.draw(m_character);

	//Set view to UI space
	window.setView(window.getDefaultView());

	//Print frame times
	//std::stringstream text;
	//text << "AMAZING MAIZY\n"
	//	<< "Graphics(ms): " << std::setw(4) << 1000.0 * m_smoothedGraphicsDelta << "\n" 
	//	<< "Haptics(ms): " << std::setw(4) << 1000.0 * m_smoothedHapticsDelta << "\n";


	float xPos = 40.0f;

	if (m_diamondcircle.found){
		m_diamondballImage.setPosition(xPos, 70.0f);
		xPos += 40;
		window.draw(m_diamondballImage);

	}
	if (m_goldencircle.found){
		m_goldenballImage.setPosition(xPos, 70.0f);
		xPos += 40;
		window.draw(m_goldenballImage);
		
	}
	if (m_silvercircle.found){
		m_silverballImage.setPosition(xPos, 70.0f);
		xPos += 40;
		window.draw(m_silverballImage);
	}
	//m_text.setString(text.str());
	//m_text.setOrigin(80, 80);
	//window.draw(m_text);
	m_timeText.setPosition((float)(window.getSize().x / 2 - m_timeText.getCharacterSize()), 0.0f);

	std::stringstream time;
	int seconds = (int)m_time;
	int minutes = seconds / 60;
	seconds %= 60;

	if (minutes < 10)
		time << "0";
	time << minutes;
	time << ":";
	if (seconds < 10)
		time << "0";
	time << seconds;
	m_timeText.setString(time.str());

	window.draw(m_timeText);

	window.draw(m_titleImage);
	m_goldencircle.UpdateGraphics();
	m_diamondcircle.UpdateGraphics();
	m_silvercircle.UpdateGraphics();

	m_highscore.Update(timeStep);
	window.draw(m_highscore);

	//Render the frame
	window.display();

	if (m_goldencircle.found  && m_diamondcircle.found && m_silvercircle.found && !gameover)
	{
		gameoverSound.setVolume(100);
		gameoverSound.play();
		gameover = true;

		int finalTime = (int)m_time;
		m_highscore.StartNewEntry(finalTime);
		m_highscore.SetIsActive(true);
	}

	if (!gameover && m_hasStarted)
	{
		m_time += timeStep;
	}
}

void HelloWorldScene::UpdateHaptics(std::vector<chai3d::cGenericHapticDevicePtr> devices, double timeStep)
{
	m_smoothedHapticsDelta += (timeStep - m_smoothedHapticsDelta) * 0.1;

	if (devices.empty())
		chai3d::cSleepMs(200);

	chai3d::cVector3d force(0,0,0);

	m_proxy.UpdateHaptics(force, devices[0], timeStep);
	b2Vec2 proxPos(m_proxy.getPosition().x, m_proxy.getPosition().y);
	b2Vec2 gf = m_goldencircle.getForce(proxPos);
	b2Vec2 sf = m_silvercircle.getForce(proxPos);
	b2Vec2 df = m_diamondcircle.getForce(proxPos);
	force(1) += gf.x + sf.x + df.x;
	force(2) += gf.y + sf.y + df.y;
	
	chai3d::cVector3d devPos;
	devices[0]->getPosition(devPos);

	force(0) -= devPos(0) * 300.0;

	devices[0]->setForce(force);

	m_lastForce.x = force.y() * 1;
	m_lastForce.y = force.z() * 1;

	if (gf.Normalize() > 0)
	{
		m_goldencircle.Move(-gf);
	}
	if (df.Normalize() > 0)
	{
		m_diamondcircle.Move(-df);
	}
	if (sf.Normalize() > 0)
	{
		m_silvercircle.Move(-sf);
	}

	playSound(proxPos);

	
	if (!m_hasStarted)
	{
		bool down = false;
		devices[0]->getUserSwitch(0, down);
		if (down)
			m_hasStarted = true;
	}
}

void HelloWorldScene::playSound(b2Vec2 proxPos)
{	
	if (!m_useSound)
		return;

	float dVol = m_diamondcircle.getSound(proxPos);
	diamondSound.setVolume(dVol);
	if (m_diamondcircle.found)
	{
		diamondSound.setVolume(0);
		diamondSound.stop();
	}
	if (!(diamondSound.getStatus() == sf::Sound::Playing) && !m_diamondcircle.found){
		diamondSound.play();
	}
	
	float sVol = m_silvercircle.getSound(proxPos);
	silverSound.setVolume(sVol);
	if (m_silvercircle.found)
	{
		silverSound.setVolume(0);
		silverSound.stop();
	}
	if (!(silverSound.getStatus() == sf::Sound::Playing) && !m_silvercircle.found){
		silverSound.play();
	}
	
	float gVol = m_goldencircle.getSound(proxPos);
	goldSound.setVolume(gVol);
	if (m_goldencircle.found)
	{
		goldSound.setVolume(0);
		goldSound.stop();
	}
	else if (!(goldSound.getStatus() == sf::Sound::Playing) && !m_goldencircle.found){
		goldSound.play();
		
	}
}


bool HelloWorldScene::checkCirclePos(b2Vec2 Pos1, b2Vec2 Pos2){
	double PosPowX = pow((abs(Pos1.x - Pos2.x)), 2);
	double PosPowY = pow((abs(Pos1.y - Pos2.y)), 2);
	double diffPos = sqrt(PosPowX + PosPowY);
	if (diffPos < 100){
		return false;
	}
	return true;
}

bool HelloWorldScene::setCirclePos(double& randGoldXpos, double& randGoldYpos, double& randSilverXpos, double& randSilverYpos, double& randDiamondXpos, double& randDiamondYpos){
	bool pos = false;
	while (!pos){
		b2Vec2 goldPos(randGoldXpos, randGoldYpos);
		b2Vec2 silverPos(randSilverXpos, randSilverYpos);
		b2Vec2 diamondPos(randDiamondXpos, randDiamondYpos);
		if (checkCirclePos(goldPos, silverPos)){
			if (checkCirclePos(diamondPos, silverPos)){
				if (checkCirclePos(goldPos, diamondPos)){
					pos = true;
				}
				else{
					randGoldXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
					randGoldYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
					randSilverXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
					randSilverYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
					randDiamondXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
					randDiamondYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));

					pos = false;
				}
			}
			else{
				randGoldXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
				randGoldYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
				randSilverXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
				randSilverYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
				randDiamondXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
				randDiamondYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));

				pos = false;
			}

		}
		else{
			randGoldXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
			randGoldYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
			randSilverXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
			randSilverYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));
			randDiamondXpos = ((rand() % 85) + 30) * (pow(-1.0, (rand() % 2 + 1)));
			randDiamondYpos = ((rand() % 45) + 30)*(pow(-1.0, (rand() % 2 + 1)));

			pos = false;
		}

	}
	return pos;
}