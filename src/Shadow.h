#pragma once

class Terrain;

class Shadow : public sf::Drawable
{
public:
	Shadow(int rays);
	~Shadow();

	void Update(const b2Vec2& position, const Terrain& terrain);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
	int m_rays;
	b2Vec2 m_position;
	
	std::unique_ptr<sf::Color[]> m_rayData;
	sf::Texture m_texture;
	sf::Shader m_shader;
	sf::RectangleShape m_fullscreenQuad;
};