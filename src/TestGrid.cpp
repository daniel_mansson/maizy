#include "pch.h"

#include "TestGrid.h"


TestGrid::TestGrid(int width, int height, double cellSize) :
	m_width(width),
	m_height(height),
	m_cellSize(cellSize)
{
	m_shapes.resize(width * height);
	m_cells.resize(width * height);

	for (int i = 0; i < height; ++i)
	{
		for (int j = 0; j < width; ++j)
		{
			sf::RectangleShape& rect = m_shapes[i * width + j];

			rect.setOrigin((float)cellSize * 0.5f, (float)cellSize * 0.5f);
			rect.setPosition(j * (float)cellSize - (float)cellSize * width * 0.5f, i * (float)cellSize - (float)cellSize * height * 0.5f);
			rect.setSize(sf::Vector2f((float)cellSize, (float)cellSize));
			rect.setFillColor((i ^ j) % 7 == 0 ? sf::Color::Red : sf::Color::White);

			m_cells[i * width + j].pos = b2Vec2((double)rect.getPosition().x, (double)rect.getPosition().y);
		}
	}



	m_proxy = sf::CircleShape(2.5f);
	m_proxy.setOrigin(m_proxy.getRadius(), m_proxy.getRadius());
}

void TestGrid::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	for (auto& rect : m_shapes)
	{
		target.draw(rect, states);
	}

	target.draw(m_proxy, states);
}

float dist(const sf::Vector2f& a, const sf::Vector2f& b)
{
	float dx = a.x - b.x;
	float dy = a.y - b.y;
	return sqrt(dx*dx+dy*dy);
}

void TestGrid::UpdateHaptics(std::vector<chai3d::cGenericHapticDevicePtr> devices, double timeStep)
{
	if (devices.empty())
		return;

	chai3d::cVector3d pos;
	devices[0]->getPosition(pos);

	b2Vec2 p(pos.y(), pos.z());
	p *= (240.0 / 0.06);
	m_proxy.setPosition(p);

	int left = (int)	((p.x - m_proxy.getRadius() + (float)m_cellSize * m_width * 0.5f) / m_cellSize);
	int right = (int)	((p.x + m_proxy.getRadius() + (float)m_cellSize * m_width * 0.5f) / m_cellSize) + 1;
	int bottom = (int)	((p.y - m_proxy.getRadius() + (float)m_cellSize * m_height * 0.5f) / m_cellSize);
	int top = (int)		((p.y + m_proxy.getRadius() + (float)m_cellSize * m_height * 0.5f) / m_cellSize) + 1;

	left = chai3d::cClamp(left, 0, m_width);
	right = chai3d::cClamp(right, 0, m_width);
	bottom = chai3d::cClamp(bottom, 0, m_height);
	top = chai3d::cClamp(top, 0, m_height);

	bool buttonDown;
	devices[0]->getUserSwitch(0, buttonDown);
	

	b2Vec2 force(0,0);
	for (int i = bottom; i < top; ++i)
	{
		for (int j = left; j < right; ++j)
		{
			if (m_shapes[i * m_width + j].getFillColor() == sf::Color::Transparent)
				continue;

			b2Vec2 to = m_cells[i * m_width + j].pos - p;
			double len = to.Normalize();

			if (len < (double)m_proxy.getRadius())
			{
				to *= 1.5 / len;
				force += ((double)m_proxy.getRadius() - len) * to;
			}

			if (len < (double)m_proxy.getRadius() * 0.7)
			{
				if (buttonDown)
					m_shapes[i * m_width + j].setFillColor(sf::Color::Transparent);
			}
		}
	}

	//std::cout << force.x << "\t" << force.y << "\n";

	pos.y(force.x);
	pos.z(force.y);
	pos.x(pos.x() * 500.0);
	devices[0]->setForce(-pos);

}