#include "pch.h"

#include "Highscore.h"


Highscore::Highscore()
	:
	m_isActive(false),
	m_current(nullptr)
{
	m_view = sf::View(sf::Vector2f(0, 0), sf::Vector2f(150, 100));

	if (!m_font.loadFromFile("resources/SourceSansPro-Regular.ttf"))
		std::cerr << "Failed to load font." << std::endl;

	m_header.setPosition(-18, -37);
	m_header.setScale(0.13f, 0.13f);
	m_header.setFont(m_font);
	m_header.setString("Highscore");
}

void Highscore::Load(const std::string& filename)
{
	m_entries.clear();

	std::ifstream file(filename);

	if (!file.is_open())
		return;

	int count;
	int time;
	std::string name;

	file >> count;
	for (int i = 0; i < count; ++i)
	{
		file >> time >> name;
		auto it = m_entries.insert(std::pair<int, Entry>(time, Entry()));
		Entry& e = it->second;
		e.name = name;
		e.time = time;
		e.nameText.setFont(m_font);
		e.timeText.setFont(m_font);

		e.nameText.setScale(0.1f, 0.1f);
		e.timeText.setScale(0.1f, 0.1f);


		e.nameText.setString(name);
		std::stringstream timestr;
		int seconds = (int)time;
		int minutes = seconds / 60;
		seconds %= 60;

		if (minutes < 10)
			timestr << "0";
		timestr << minutes;
		timestr << ":";
		if (seconds < 10)
			timestr << "0";
		timestr << seconds;
		e.timeText.setString(timestr.str());
	}
}

void Highscore::Save(const std::string& filename)
{
	std::ofstream file(filename);

	file << m_entries.size() << "\n";

	for (const auto& e : m_entries)
	{
		file << e.second.time << "\n"; 
		file << e.second.name << "\n"; 
	}
}

void Highscore::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if (!IsActive())
		return;

	sf::View oldView = target.getView();
	target.setView(m_view);

	for (const auto& e : m_entries)
	{
		target.draw(e.second.nameText);
		target.draw(e.second.timeText);
	}

	target.draw(m_header);

	target.setView(oldView);
}

void Highscore::Update(double timeStep)
{

	int i = 0;
	for (auto& e : m_entries)
	{
		e.second.nameText.setPosition(-15 - 3, (float)(-30 + i * 3.5f));
		e.second.timeText.setPosition( 15 - 3, (float)(-30 + i * 3.5f));

		++i;
	}

	if (m_current != nullptr)
	{
		m_current->nameText.setString(m_current->name);

		std::stringstream time;
		int seconds = (int)m_current->time;
		int minutes = seconds / 60;
		seconds %= 60;

		if (minutes < 10)
			time << "0";
		time << minutes;
		time << ":";
		if (seconds < 10)
			time << "0";
		time << seconds;
		m_current->timeText.setString(time.str());
	}
}

bool Highscore::HandleEvent(sf::Event event)
{
	if (m_current != nullptr)
	{
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Return)
			{
				m_current->nameText.setColor(sf::Color::White);
				m_current->timeText.setColor(sf::Color::White);
				m_current = nullptr;
				return true;
			}

			if (event.key.code >= sf::Keyboard::A && 
				event.key.code <= sf::Keyboard::Z &&
				m_current->name.size() < 12)
			{
				m_current->name += ('A' + event.key.code);
			}

			if (event.key.code == sf::Keyboard::BackSpace)
			{
				if (!m_current->name.empty())
					m_current->name.pop_back();
			}
		}
	}

	return false;
}

void Highscore::StartNewEntry(int time)
{
	if (!m_entries.empty() && m_entries.size() >= 20)
	{
		auto lastIt = m_entries.end();
		--lastIt;

		if (time >= lastIt->second.time)
			return;
	}

	auto it = m_entries.insert(std::pair<int, Entry>(time, Entry()));
	m_current = &it->second;

	m_current->time = time;
	m_current->nameText.setFont(m_font);
	m_current->timeText.setFont(m_font);
	m_current->nameText.setColor(sf::Color::Red);
	m_current->timeText.setColor(sf::Color::Red);
	m_current->nameText.setScale(0.1f, 0.1f);
	m_current->timeText.setScale(0.1f, 0.1f);

	if (m_entries.size() > 20)
	{
		auto lastIt = m_entries.end();
		--lastIt;
		m_entries.erase(lastIt);
	}
}
