#pragma once

class ResourceManager;

class Global
{
	//Static
public:
	static Global& Instance();
	static void Destroy();
private:
	static std::unique_ptr<Global> s_instance;

	//Impl
public:
	ResourceManager& GetResourceManager();

	~Global();
private:
	Global();
	Global(const Global&);

	std::unique_ptr<ResourceManager> m_resourceManager;
};