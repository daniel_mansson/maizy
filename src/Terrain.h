#pragma once

class Terrain : public sf::Drawable, public sf::Transformable
{
public:
	struct Cell
	{
		b2Vec2 pos;
		float durability;
	};

	Terrain(int width, int height, double cellSize);

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void UpdateCells(const b2Vec2& pos, double radius, std::function<void(Cell&, const b2Vec2&, double)> callback);
	void UpdateTexture(const b2Vec2& pos, double radius);
	void UpdateTexture(const std::vector<std::pair<b2Vec2, double>>& areas);
	void UpdateTexture(int left, int right, int bottom, int top);

	bool RayCast(const b2Vec2& from, const b2Vec2& dir, double& distance, double threshold = 0.01f, int maxSteps = 500) const;

private:
	int m_width;
	int m_height;
	double m_cellSize;

	sf::Texture m_stoneTexture;
	sf::Texture m_texture;
	sf::Sprite m_sprite;
	sf::Shader m_shader;

	std::vector<Cell> m_cells;
	std::unique_ptr<sf::Color[]> m_pixels;
};