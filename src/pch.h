#pragma once

#include <memory>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <chai3d.h>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <set>
#include <algorithm>
#include <math.h>
#include <iomanip>
#include <stdlib.h>    
#include <time.h> 
#include <math.h>
#include <thread>

#include "Box2D/b2Math.h"
#include "Box2D/b2Settings.h"

namespace sf
{
	typedef Vector2<double> Vector2d;
};