#include "pch.h"

#include "Application.h"

int main()
{
	Application app;

	if (!app.Initialize())
	{
		std::cerr << "Initialization failed!" << std::endl;
		return 1;
	}

	app.Run();

	return 0;
}


