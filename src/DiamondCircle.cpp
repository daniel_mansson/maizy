#include "pch.h"
#include "DiamondCircle.h"


sf::Color diamond(185, 242, 255);

DiamondCircle::DiamondCircle(Terrain& terrain) :
m_terrain(terrain)
{
	//std::cout << "circle" << std::endl;
}

void DiamondCircle::setValues(float x, float y)
{
	m_dcshape.setOrigin(5, 5);
	m_dcshape.setRadius(5);
	m_dcshape.setFillColor(diamond);
	m_dcshape.setPosition(x, y);


}

void DiamondCircle::clearTerrain() {
	double radius = m_dcshape.getRadius() + 1.5;
	//std::cout << "circle" << std::endl;
	m_terrain.UpdateCells(m_dcshape.getPosition(), radius, [](Terrain::Cell& cell, const b2Vec2& dir, double length)
	{
		cell.durability = 0;
	});
	m_terrain.UpdateTexture(m_dcshape.getPosition(), radius);
}

sf::CircleShape DiamondCircle::getCircle()
{
	return m_dcshape;
}

void DiamondCircle::UpdateGraphics()
{
	b2Vec2 position = m_dcshape.getPosition();
	double len = (b2Vec2(0, 0) - position).Normalize();
	if (len < 10)
	{
		m_dcshape.setFillColor(sf::Color(0, 0, 0,0));
		found = true;

	}


}

void DiamondCircle::UpdateHaptics(chai3d::cVector3d& force, double timeStep)
{

}


b2Vec2  DiamondCircle::getForce(const b2Vec2 proxPos)
{
	if (found) return b2Vec2(0, 0);
	b2Vec2 f(0, 0);
	b2Vec2 dcPos(m_dcshape.getPosition().x, m_dcshape.getPosition().y);
	double PosPowX = pow((abs(dcPos.x - proxPos.x)), 2);
	double PosPowY = pow((abs(dcPos.y - proxPos.y)), 2);
	double diffPos = sqrt(PosPowX + PosPowY);

	b2Vec2 dir(proxPos.x - m_dcshape.getPosition().x, proxPos.y - m_dcshape.getPosition().y);

	if (diffPos < (m_dcshape.getRadius() + 3)){
		f = 0.7  * (m_dcshape.getRadius() + 3 - diffPos) * dir;

	}

	return f;

}

float DiamondCircle::getSound(const b2Vec2 proxPos){
	
	b2Vec2 dcPos(m_dcshape.getPosition().x, m_dcshape.getPosition().y);
	double PosPowX = pow((abs(dcPos.x-proxPos.x)),2) ;
	double PosPowY = pow((abs(dcPos.y - proxPos.y)), 2);
	double diffPos = sqrt(PosPowX + PosPowY);

	double vol = 100 - (diffPos * 2);
	if (vol < 0) vol = 0;
	return (float)vol;
}

void DiamondCircle::Move(b2Vec2 vec)
{
	b2Vec2 m_position = m_dcshape.getPosition();

	double len = vec.Normalize();

	const double stepLength = 0.005;
	const int maxSteps = 100;

	double momentum = 1.0;
	double radius = m_dcshape.getRadius();

	for (int i = 0; i < maxSteps; ++i)
	{
		m_terrain.UpdateCells(m_position, radius, [&momentum, vec, radius](Terrain::Cell& cell, const b2Vec2& dir, double length)
		{
			double d = b2Dot(vec, dir);

			if (d > 0)
				momentum -= d * 0.8 * (radius - length) * cell.durability;
		});

		if (momentum < 0)
			break;

		double step = stepLength * momentum;
		if (step > len)
			step = len;

		m_position += step * vec;
		len -= step;

		if (len < 0.000001)
			break;
	}

	m_dcshape.setPosition(m_position);
	//m_deviceCircle.setPosition(m_devicePos - m_position);
}
