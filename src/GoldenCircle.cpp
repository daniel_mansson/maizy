#include "pch.h"
#include "GoldenCircle.h"

sf::Color gold(255, 215, 0);

GoldenCircle::GoldenCircle(Terrain& terrain) :
m_terrain(terrain)
{
	
	found = false;
}

void GoldenCircle::clearTerrain() {
	double radius = m_gcshape.getRadius() + 1.5;
	//std::cout << "circle" << std::endl;
	m_terrain.UpdateCells(m_gcshape.getPosition(), radius, [](Terrain::Cell& cell, const b2Vec2& dir, double length)
	{
			cell.durability = 0;
	});
	m_terrain.UpdateTexture(m_gcshape.getPosition(), radius);
}

void GoldenCircle::setValues( float x, float y )
{

	m_gcshape.setOrigin(4,4);
	m_gcshape.setRadius(4);
	m_gcshape.setFillColor(gold);
	m_gcshape.setPosition(x, y);
}

sf::CircleShape GoldenCircle::getCircle()
{
	return m_gcshape;
}
void GoldenCircle::UpdateGraphics() 
{
	b2Vec2 position = m_gcshape.getPosition();
	double len = (b2Vec2(0, 0) - position).Normalize();
	if (len < 10) 
	{
		m_gcshape.setFillColor(sf::Color(0,0,0,0));
		found = true;

	}
}


void GoldenCircle::UpdateHaptics(chai3d::cVector3d& force, double timeStep)
{

}


b2Vec2  GoldenCircle::getForce(const b2Vec2 proxPos)
{
	if (found) return b2Vec2(0, 0);
	b2Vec2 f(0,0);
	b2Vec2 dcPos(m_gcshape.getPosition().x, m_gcshape.getPosition().y);
	double PosPowX = pow((abs(dcPos.x - proxPos.x)), 2);
	double PosPowY = pow((abs(dcPos.y - proxPos.y)), 2);
	double diffPos = sqrt(PosPowX + PosPowY);

	b2Vec2 dir(proxPos.x - m_gcshape.getPosition().x, proxPos.y - m_gcshape.getPosition().y);

	if (diffPos < (m_gcshape.getRadius() + 3)){
		f = 0.7  * (m_gcshape.getRadius() + 3 - diffPos) * dir;

	}
	return f;

}

float GoldenCircle::getSound(const b2Vec2 proxPos){

	b2Vec2 dcPos(m_gcshape.getPosition().x, m_gcshape.getPosition().y);
	double PosPowX = pow((abs(dcPos.x - proxPos.x)), 2);
	double PosPowY = pow((abs(dcPos.y - proxPos.y)), 2);
	double diffPos = sqrt(PosPowX + PosPowY);

	double vol = 100 - (diffPos * 2);
	if (vol < 0) vol = 0;
	return (float)vol;
}

void GoldenCircle::Move(b2Vec2 vec)
{
	b2Vec2 m_position = m_gcshape.getPosition();

	double len = vec.Normalize();

	const double stepLength = 0.005;
	const int maxSteps = 100;

	double momentum = len;
	double radius = m_gcshape.getRadius();

	for (int i = 0; i < maxSteps; ++i)
	{
		m_terrain.UpdateCells(m_position, radius, [&momentum, vec, radius](Terrain::Cell& cell, const b2Vec2& dir, double length)
		{
			double d = b2Dot(vec, dir);

			if (d > 0)
				momentum -= d * 0.8 * (radius - length) * cell.durability;
		});

		if (momentum < 0)
			break;

		double step = stepLength * momentum;
		if (step > len)
			step = len;

		m_position += step * vec;
		len -= step;

		if (len < 0.000001)
			break;
	}

	m_gcshape.setPosition(m_position);
	//m_deviceCircle.setPosition(m_devicePos - m_position);
}