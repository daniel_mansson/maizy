#pragma once

#include "Scene.h"
#include "TestGrid.h"
#include "Terrain.h"
#include "Proxy.h"
#include "Circle.h"
#include "GoldenCircle.h"
#include "SilverCircle.h"
#include "DiamondCircle.h"
#include "Shadow.h"
#include "Character.h"
#include "Highscore.h"

class HelloWorldScene : public Scene
{
public :
	HelloWorldScene(SceneStack& sceneStack);
	void playSound(b2Vec2);
	bool checkCirclePos(b2Vec2 Pos1, b2Vec2 Pos2);
	bool setCirclePos(double&, double&, double&, double&, double&, double&);
	virtual ~HelloWorldScene();

	virtual void OnEnter(ScenePtr from);
	virtual void OnExit(ScenePtr to);

	virtual void HandleEvent(sf::Event event);
	virtual void UpdateGraphics(sf::RenderWindow& window, double timeStep);
	virtual void UpdateHaptics(std::vector<chai3d::cGenericHapticDevicePtr> devices, double timeStep);
	

private:
	sf::Text m_text;
	sf::Font m_font;
	bool m_useSound;

	TestGrid m_grid;
	Terrain m_terrain;
	Proxy m_proxy;
	b2Vec2 m_lastForce;
	sf::Sprite m_background;
	sf::CircleShape m_chest;
	sf::CircleShape m_goldenballImage;
	sf::CircleShape m_silverballImage;
	sf::CircleShape m_diamondballImage;
	sf::RectangleShape m_titleImage;
	sf::Texture m_chestTexture;
	sf::Texture m_backgroundTexture;
	sf::Texture m_titleTexture;
	sf::Texture m_goldenballTexture;
	sf::Texture m_silverballTexture;
	sf::Texture m_diamondballTexture;
	sf::Shader m_backgroundShader;
	sf::RenderStates m_backgroundStates;
	sf::Text m_timeText;
	Character m_character;

	double m_time;
	double m_smoothedHapticsDelta;
	double m_smoothedGraphicsDelta;

	bool gameover;
	
	sf::SoundBuffer dBuffer;
	sf::Sound diamondSound;
	sf::SoundBuffer sBuffer;
	sf::Sound silverSound;
	sf::SoundBuffer gBuffer;
	sf::Sound goldSound;
	sf::SoundBuffer gameoverBuffer;
	sf::Sound gameoverSound;

	
	Shadow m_shadow;

	GoldenCircle m_goldencircle;
	SilverCircle m_silvercircle;
	DiamondCircle m_diamondcircle;

	Highscore m_highscore;
	bool m_displayShadows;
	bool m_hasStarted;
	bool m_closeUp;

};