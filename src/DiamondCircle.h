#pragma once

#include "Terrain.h"

class DiamondCircle
{
public:
	DiamondCircle(Terrain& terrain);
	void UpdateHaptics(chai3d::cVector3d& force, double timeStep);
	void UpdateGraphics();
	void Move(b2Vec2 vec);
	void clearTerrain();
	void setValues( float x, float y);
	sf::CircleShape getCircle();
	b2Vec2 getForce(const b2Vec2);
	float getSound(const b2Vec2);
	bool found;

private:
	Terrain& m_terrain;
protected:
	sf::CircleShape m_dcshape;

};