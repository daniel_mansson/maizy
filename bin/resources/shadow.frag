uniform sampler2D shadowTex;
uniform float radius;
uniform vec2 position;


void main()
{
	//gl_FragColor = vec4(texture2D(shadowTex, vec2(gl_TexCoord[0].x, 0)).x, 0.0, 0.0, 0.8);
	
	vec2 t = gl_TexCoord[0].xy;
	vec2 vec = t - position;
	
	float p = atan(vec.y, vec.x) / (2 * 3.14159);
	
	//float m = texture2D(shadowTex, vec2(p, 0)).x;
	float m1 = (1.3/8.0)* texture2D(shadowTex, vec2(p, 0)).x;
	float m2 = (1.3/8.0)* texture2D(shadowTex, vec2(p + 0.008, 0)).x;
	float m3 = (1.3/8.0)* texture2D(shadowTex, vec2(p - 0.008, 0)).x;
	
	float d = sqrt(vec.x * vec.x + vec.y * vec.y);
	
	float a = 0;
	
	//Shadows	
	if(d > m1)
		a += min(200*(d - m1),0.85);
	if(d > m2)
		a += min(200*(d - m2),0.85);
	if(d > m3)
		a += min(200*(d - m3),0.85);
	a /= 3;
	
	//Nearby light
	a += min((-0.012f + d) / 0.006f, 0);
	a -= (0.015f / (d*2.0f + 0.06f));
	
	//Diminishing light
	//a += 1 - 0.05 / (d * 0.5 + 0.05);	
	a += 8 * d;
	
	/*float d = sqrt(vec.x * vec.x + vec.y * vec.y);
	
	float a;
	if(d < m)
		a = 0.0;
	else
		a = min(80*(d - m),0.85);
		
		*/
	gl_FragColor = vec4(0, 0, 0, a);

}
