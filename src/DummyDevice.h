#pragma once

class DummyDevice : public chai3d::cGenericHapticDevice
{
public:
	DummyDevice(sf::RenderWindow& window);

	void UpdateGraphics(double timeStep);
	void UpdateHaptics(double timeStep);

	bool getPosition(chai3d::cVector3d& a_position) { a_position = m_position; return true; }

	bool getUserSwitch(int a_switchIndex, bool& status);

private:
	sf::RenderWindow& m_window;

	chai3d::cVector3d m_fromPos;
	chai3d::cVector3d m_toPos;
	chai3d::cVector3d m_position;
	double m_t;
	double m_timeStep;
};