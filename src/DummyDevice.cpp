#include "pch.h"

#include "DummyDevice.h"

DummyDevice::DummyDevice(sf::RenderWindow& window) :
m_window(window),
m_t(0),
m_timeStep(0.1),
m_fromPos(0, 0, 0),
m_toPos(0, 0, 0),
m_position(0, 0, 0)
{

}

void DummyDevice::UpdateGraphics(double timeStep)
{
	double t = m_t / m_timeStep;
	m_fromPos = m_fromPos * (1 - t) + m_toPos * t;
	m_position = m_fromPos;

	sf::Vector2i pos = sf::Mouse::getPosition(m_window);
	m_toPos = chai3d::cVector3d( 
		0.0,
		0.033 * 2.0 * ((double)pos.x / (double)m_window.getSize().x - 0.5),
		-(0.033 * ((double)m_window.getSize().y / (double)m_window.getSize().x)) * 2.0 * ((double)pos.y / (double)m_window.getSize().y - 0.5));

	m_t = 0.0;
	m_timeStep = timeStep;

}

void DummyDevice::UpdateHaptics(double timeStep)
{
	m_t = chai3d::cClamp01(m_t + timeStep);

	double t = m_t / m_timeStep;
	m_position = m_fromPos * (1 - t) + m_toPos * t;
}


bool DummyDevice::getUserSwitch(int a_switchIndex, bool& status)
{
	status = sf::Mouse::isButtonPressed((sf::Mouse::Button)a_switchIndex);
	return true;
}
