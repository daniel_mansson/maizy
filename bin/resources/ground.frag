uniform sampler2D texture;

uniform float value;

void main()
{
    gl_FragColor = texture2D(texture, 2.0f * gl_TexCoord[0].xy) * gl_Color;
}

