#include "pch.h"

#include "Character.h"


Character::Character()
	:
	m_tileSize(0),
	m_currentSprite(0),
	m_speed(1.0),
	m_dir(0)
{

}

void Character::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(m_sprite, states);
}

void Character::setImage(const sf::Image& image, int tileSize)
{ 
	m_tileSize = tileSize;
	m_texture.loadFromImage(image);
	m_sprite.setTexture(m_texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, tileSize, tileSize));
	m_sprite.setOrigin((float)(tileSize * 0.5), (float)(tileSize * 0.8));
	m_sprite.setScale(7.0f / tileSize, -7.0f / tileSize);
}

void Character::update(double timeStep)
{
	m_time += timeStep * m_speed;
	if (m_time > 1.0)
	{
		m_time -= 1.0;
		m_currentSprite = (m_currentSprite + 1) % 3;

		m_sprite.setTextureRect(sf::IntRect(m_currentSprite * m_tileSize, m_dir * m_tileSize, m_tileSize, m_tileSize));
	}
}

void Character::setDirection(const b2Vec2& direction)
{
	int dir = (int)(8 * (b2Atan2(direction.y, direction.x) + b2_pi) / (2.0 * b2_pi) + 0.5);

	switch (dir)
	{
	case 8:
	case 0: // 
		m_dir = 1;
		break;
	case 1: // 
		m_dir = 4;
		break;
	case 2: // 
		m_dir = 0;
		break;
	case 3: // 
		m_dir = 6;
		break;
	case 4: // 
		m_dir = 2;
		break;
	case 5: // 
		m_dir = 7;
		break;
	case 6: // 
		m_dir = 3;
		break;
	case 7: // 
		m_dir = 5;
		break;
	}
	m_sprite.setTextureRect(sf::IntRect(m_currentSprite * m_tileSize, m_dir * m_tileSize, m_tileSize, m_tileSize));

}