uniform sampler2D texture;
uniform sampler2D stone;

uniform float value;

void main()
{
    vec4 c = texture2D(texture, gl_TexCoord[0].xy) * gl_Color;
	
	if(c.a > 0.9)
		gl_FragColor = texture2D(stone, 2 * gl_TexCoord[0].xy) * gl_Color;//vec4(c.r, c.g, c.b, 1.0);
	else if(c.a > 0.1)
	{
		float v = 2 * (1.0 - c.a) / 0.6;
		gl_FragColor = texture2D(stone, 2 * gl_TexCoord[0].xy) * gl_Color * vec4(v, v, v, 1);
	}
	else
		gl_FragColor = vec4(1,1,1, c.a / 0.1);
}

