#include "pch.h"

#include "Global.h"
#include "ResourceManager.h"

/**** Static ****/
std::unique_ptr<Global> Global::s_instance = nullptr;

Global& Global::Instance()
{
	if (s_instance == nullptr)
		s_instance = std::unique_ptr<Global>(new Global());

	return *s_instance;
}

void Global::Destroy()
{
	s_instance = nullptr;
}

/**** Impl ****/
Global::Global():
	m_resourceManager(new ResourceManager())
{
}

Global::Global(const Global&)
{
}

Global::~Global()
{
}

ResourceManager& Global::GetResourceManager()
{
	return *m_resourceManager;
}